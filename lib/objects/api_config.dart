/*
 * Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * SPDX-License-Identifier: MIT-0
 */

class ApiConfig {
  // Format: https://<api-id>.execute-api.<aws-region-id>.amazonaws.com/Prod/
  static String get apiUrl => "https://e5fd88dab6.execute-api.ap-southeast-1.amazonaws.com/Prod/"; // API url goes here
  static String get region => "ap-southeast-1"; // Add region here
}
