import 'package:flutter_aws_chime_plugin/objects/method_channel_coordinator.dart';
import 'package:flutter_aws_chime_plugin/view_models/join_meeting_view_model.dart';
import 'package:flutter_aws_chime_plugin/view_models/meeting_view_model.dart';
import 'package:provider/provider.dart';

class FlutterAWSChime {
  ChangeNotifierProvider<MethodChannelCoordinator>
      initializeMethodCoordinator() {
    return ChangeNotifierProvider(create: (_) => MethodChannelCoordinator());
  }

  ChangeNotifierProvider<JoinMeetingViewModel> iniatilizeJoinMeetingModel() {
    return ChangeNotifierProvider(create: (_) => JoinMeetingViewModel());
  }

  ChangeNotifierProvider<MeetingViewModel> initializeMeetingViewModel() {
    return ChangeNotifierProvider(
        create: (context) => MeetingViewModel(context));
  }
}
