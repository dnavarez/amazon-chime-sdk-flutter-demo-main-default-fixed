/*
 * Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * SPDX-License-Identifier: MIT-0
 */

import 'package:flutter/material.dart';
import 'package:flutter_aws_chime_plugin/interfaces/audio_video_devices_interface.dart';
import 'package:flutter_aws_chime_plugin/interfaces/audio_video_interface.dart';
import 'package:flutter_aws_chime_plugin/interfaces/video_tile_interface.dart';
import 'package:flutter_aws_chime_plugin/objects/response_enums.dart';
import 'package:flutter_aws_chime_plugin/interfaces/realtime_interface.dart';
import 'package:provider/provider.dart';
import '../objects/attendee.dart';
import '../objects/logger.dart';

import '../objects/api.dart';
import '../objects/method_channel_coordinator.dart';
import '../objects/video_tile.dart';

class MeetingViewModel extends ChangeNotifier
    implements
        RealtimeInterface,
        VideoTileInterface,
        AudioDevicesInterface,
        AudioVideoInterface {
  String? meetingId;

  JoinInfo? meetingData;

  MethodChannelCoordinator? methodChannelProvider;

  String? localAttendeeId;
  String? remoteAttendeeId;
  String? contentAttendeeId;

  String? selectedAudioDevice;
  String? selectedVideoDevice;
  String? selectedVideoFormat;

  List<String?> audioDeviceList = [];
  List<String?> videoDeviceList = [];
  List<String?> videoFormatList = [];

  List<String?> bgBlurStrengthList = [];
  String? selectedVideoFilter;

  // AttendeeId is the key
  Map<String, Attendee> currAttendees = {};

  bool isReceivingScreenShare = false;
  bool isMeetingActive = false;
  bool isNoiseCancellationEnabled = false;
  bool isMicOn = true;
  bool isVideoOn = false;

  MeetingViewModel(BuildContext context) {
    methodChannelProvider =
        Provider.of<MethodChannelCoordinator>(context, listen: false);
  }

  //
  // ————————————————————————— Initializers —————————————————————————
  //

  void setMicState() {
    isMicOn = !isMicOn;
    notifyListeners();
  }

  void setNoiseCancellationState(bool isEnabled) {
    isNoiseCancellationEnabled = isEnabled;
    notifyListeners();
  }

  void setVideoOn() {
    isVideoOn = !isVideoOn;
    notifyListeners();
  }

  void switchCamera() async {
    MethodChannelResponse? resp = await methodChannelProvider
        ?.callMethod(MethodCallOption.switchCamera);

    if (resp == null) {
      logger.e(Response.speaker_test_null);
      return;
    }

    logger.i("${resp.arguments}");
    notifyListeners();
  }

  void intializeMeetingData(JoinInfo meetData) {
    isMeetingActive = true;
    meetingData = meetData;
    meetingId = meetData.meeting.externalMeetingId;
    notifyListeners();
  }

  void initializeLocalAttendee() {
    if (meetingData == null) {
      logger.e(Response.null_meeting_data);
      return;
    }
    localAttendeeId = meetingData!.attendee.attendeeId;

    if (localAttendeeId == null) {
      logger.e(Response.null_local_attendee);
      return;
    }
    currAttendees[localAttendeeId!] =
        Attendee(localAttendeeId!, meetingData!.attendee.externalUserId);
    notifyListeners();
  }

  //
  // ————————————————————————— Interface Methods —————————————————————————
  //

  @override
  Future<void> listBackgroundBlurStrength() async {
    MethodChannelResponse? bgBlurStrengths = await methodChannelProvider
        ?.callMethod(MethodCallOption.listBackgroundBlurStrength);

    if (bgBlurStrengths == null) {
      logger.e(Response.null_audio_device_list);
      return;
    }

    final strengthIterable = bgBlurStrengths.arguments.map((strength) => strength.toString());

    final strengthList = List<String?>.from(strengthIterable.toList());
    strengthList.insert(0, "Off");
    logger.d("Background blur strength available: $strengthList");
    bgBlurStrengthList = strengthList;

    notifyListeners();
  }

  // Note: For the moment, we only apply blur effects for video filter
  @override
  void updateVideoFilter(String videoFilter) async {
    MethodChannelResponse? resp = await methodChannelProvider
        ?.callMethod(MethodCallOption.updateBackgroundBlurStrength, videoFilter);

    if (resp == null) {
      logger.e(Response.null_video_filter_update);
      return;
    }

    if (resp.result) {
      logger.i("${resp.arguments} to: $resp");
      selectedVideoFilter = videoFilter;
    } else {
      logger.e("${resp.arguments}");
      selectedVideoFilter = null;
    }

    notifyListeners();
  }

  @override
  void attendeeDidPrepare(Attendee attendee) {}

  @override
  void attendeeDidJoin(Attendee attendee) {
    String? attendeeIdToAdd = attendee.attendeeId;
    if (_isAttendeeContent(attendeeIdToAdd)) {
      logger.i("Content detected");
      contentAttendeeId = attendeeIdToAdd;
      if (contentAttendeeId != null) {
        currAttendees[contentAttendeeId!] = attendee;
        logger.i("Content added to the meeting");
      }
      notifyListeners();
      return;
    }

    if (attendeeIdToAdd != localAttendeeId) {
      remoteAttendeeId = attendeeIdToAdd;
      if (remoteAttendeeId == null) {
        logger.e(Response.null_remote_attendee);
        return;
      }
      currAttendees[remoteAttendeeId!] = attendee;
      logger.i(
          "${formatExternalUserId(currAttendees[remoteAttendeeId]?.externalUserId)} has joined the meeting.");
      notifyListeners();
    }
  }

  // Used for both leave and drop callbacks
  @override
  void attendeeDidLeave(Attendee attendee, {required bool didDrop}) {
    final attIdToDelete = attendee.attendeeId;
    currAttendees.remove(attIdToDelete);
    if (didDrop) {
      logger.i(
          "${formatExternalUserId(attendee.externalUserId)} has dropped from the meeting");
    } else {
      logger.i(
          "${formatExternalUserId(attendee.externalUserId)} has left the meeting");
    }
    notifyListeners();
  }

  @override
  void attendeeDidMute(Attendee attendee) {
    _changeMuteStatus(attendee, mute: true);
  }

  @override
  void attendeeDidUnmute(Attendee attendee) {
    _changeMuteStatus(attendee, mute: false);
  }

  @override
  void videoTileDidAdd(String attendeeId, VideoTile videoTile) {
    currAttendees[attendeeId]?.videoTile = videoTile;
    if (videoTile.isContentShare) {
      isReceivingScreenShare = true;
      notifyListeners();
      return;
    }
    currAttendees[attendeeId]?.isVideoOn = true;
    notifyListeners();
  }

  @override
  void videoTileDidRemove(String attendeeId, VideoTile videoTile) {
    if (videoTile.isContentShare) {
      currAttendees[contentAttendeeId]?.videoTile = null;
      isReceivingScreenShare = false;
    } else {
      currAttendees[attendeeId]?.videoTile = null;
      currAttendees[attendeeId]?.isVideoOn = false;
    }
    notifyListeners();
  }

  @override
  Future<void> initialAudioSelection() async {
    MethodChannelResponse? device = await methodChannelProvider
        ?.callMethod(MethodCallOption.initialAudioSelection);
    if (device == null) {
      logger.e(Response.null_initial_audio_device);
      return;
    }
    logger.i("Initial audio device selection: ${device.arguments}");
    selectedAudioDevice = device.arguments;
    notifyListeners();
  }

  @override
  Future<void> listAudioDevices() async {
    MethodChannelResponse? devices = await methodChannelProvider
        ?.callMethod(MethodCallOption.listAudioDevices);

    if (devices == null) {
      logger.e(Response.null_audio_device_list);
      return;
    }
    final deviceIterable = devices.arguments.map((device) => device.toString());

    final devList = List<String?>.from(deviceIterable.toList());
    logger.d("Devices available: $devList");
    audioDeviceList = devList;
    notifyListeners();
  }

  @override
  Future<void> listVideoDevices() async {
    MethodChannelResponse? devices = await methodChannelProvider
        ?.callMethod(MethodCallOption.listVideoDevices);

    if (devices == null) {
      logger.e(Response.null_video_device_list);
      return;
    }
    final deviceIterable = devices.arguments.map((device) => device.toString());

    final devList = List<String?>.from(deviceIterable.toList());
    logger.d("Devices available: $devList");
    videoDeviceList = devList;
    notifyListeners();
  }

  @override
  Future<void> listVideoFormats() async {
    MethodChannelResponse? deviceFormats = await methodChannelProvider
        ?.callMethod(MethodCallOption.listVideoFormats);

    if (deviceFormats == null) {
      logger.e(Response.null_video_format_list);
      return;
    }
    final deviceIterable =
        deviceFormats.arguments.map((deviceFormat) => deviceFormat.toString());

    final devFormatList = List<String?>.from(deviceIterable.toList());
    logger.d("Video formats available: $devFormatList");
    videoFormatList = devFormatList;
    notifyListeners();
  }

  @override
  void updateCurrentAudioDevice(String device) async {
    MethodChannelResponse? updateDeviceResponse = await methodChannelProvider
        ?.callMethod(MethodCallOption.updateAudioDevice, device);

    if (updateDeviceResponse == null) {
      logger.e(Response.null_audio_device_update);
      return;
    }

    if (updateDeviceResponse.result) {
      logger.i("${updateDeviceResponse.arguments} to: $device");
      selectedAudioDevice = device;
      notifyListeners();
    } else {
      logger.e("${updateDeviceResponse.arguments}");
    }
  }

  @override
  void updateCurrentVideoDevice(String device) async {
    MethodChannelResponse? updateDeviceResponse = await methodChannelProvider
        ?.callMethod(MethodCallOption.updateVideoDevice, device);

    if (updateDeviceResponse == null) {
      logger.e(Response.null_video_device_update);
      return;
    }

    if (updateDeviceResponse.result) {
      logger.i("${updateDeviceResponse.arguments} to: $device");
      selectedVideoDevice = device;
      notifyListeners();
    } else {
      logger.e("${updateDeviceResponse.arguments}");
    }
  }

  @override
  void testSpeaker() async {
    MethodChannelResponse? speakerTestResponse =
        await methodChannelProvider?.callMethod(MethodCallOption.speakerTest);

    if (speakerTestResponse == null) {
      logger.e(Response.speaker_test_null);
      return;
    }

    logger.i("${speakerTestResponse.arguments}");
    notifyListeners();
  }

  @override
  void updateNoiseCancellationStatus() async {
    String status = isNoiseCancellationEnabled ? "enable" : "disable";

    Map<String, dynamic> args = {
      "status": status,
    };

    MethodChannelResponse? updateDeviceResponse = await methodChannelProvider
        ?.callMethod(MethodCallOption.noiseCancellation, args);

    if (updateDeviceResponse == null) {
      logger.e(Response.null_video_device_update);
      return;
    }

    if (updateDeviceResponse.result) {
      logger.i("${updateDeviceResponse.arguments} to: $status");
      notifyListeners();
    } else {
      logger.e("${updateDeviceResponse.arguments}");
    }
  }

  @override
  void updateCurrentVideoFormat(String videoFormat) async {
    MethodChannelResponse? updateDeviceResponse = await methodChannelProvider
        ?.callMethod(MethodCallOption.updateVideoFormat, videoFormat);

    if (updateDeviceResponse == null) {
      logger.e(Response.null_video_format_update);
      return;
    }

    if (updateDeviceResponse.result) {
      logger.i("${updateDeviceResponse.arguments} to: $videoFormat");
      selectedVideoFormat = videoFormat;
      notifyListeners();
    } else {
      logger.e("${updateDeviceResponse.arguments}");
    }
  }

  @override
  void audioSessionDidStop() {
    logger.i("Audio session stopped by AudioVideoObserver.");
    _resetMeetingValues();
  }

  //
  // —————————————————————————— Methods ——————————————————————————————————————
  //

  void _changeMuteStatus(Attendee attendee, {required bool mute}) {
    final attIdToggleMute = attendee.attendeeId;
    currAttendees[attIdToggleMute]?.muteStatus = mute;
    if (mute) {
      logger
          .i("${formatExternalUserId(attendee.externalUserId)} has been muted");
    } else {
      logger.i(
          "${formatExternalUserId(attendee.externalUserId)} has been unmuted");
    }
    notifyListeners();
  }

  void sendLocalMuteToggle() async {
    if (!currAttendees.containsKey(localAttendeeId)) {
      logger.e("Local attendee not found");
      return;
    }

    if (currAttendees[localAttendeeId]!.muteStatus) {
      MethodChannelResponse? unmuteResponse =
          await methodChannelProvider?.callMethod(MethodCallOption.unmute);
      if (unmuteResponse == null) {
        logger.e(Response.unmute_response_null);
        return;
      }

      if (unmuteResponse.result) {
        logger.i(
            "${unmuteResponse.arguments} ${formatExternalUserId(currAttendees[localAttendeeId]?.externalUserId)}");
        notifyListeners();
      } else {
        logger.e(
            "${unmuteResponse.arguments} ${formatExternalUserId(currAttendees[localAttendeeId]?.externalUserId)}");
      }
    } else {
      MethodChannelResponse? muteResponse =
          await methodChannelProvider?.callMethod(MethodCallOption.mute);
      if (muteResponse == null) {
        logger.e(Response.mute_response_null);
        return;
      }

      if (muteResponse.result) {
        logger.i(
            "${muteResponse.arguments} ${formatExternalUserId(currAttendees[localAttendeeId]?.externalUserId)}");
        notifyListeners();
      } else {
        logger.e(
            "${muteResponse.arguments} ${formatExternalUserId(currAttendees[localAttendeeId]?.externalUserId)}");
      }
    }
  }

  void sendLocalVideoTileOn() async {
    if (!currAttendees.containsKey(localAttendeeId)) {
      logger.e("Local attendee not found");
      return;
    }

    if (currAttendees[localAttendeeId]!.isVideoOn) {
      MethodChannelResponse? videoStopped = await methodChannelProvider
          ?.callMethod(MethodCallOption.localVideoOff);
      if (videoStopped == null) {
        logger.e(Response.video_stopped_response_null);
        return;
      }

      if (videoStopped.result) {
        logger.i(videoStopped.arguments);
      } else {
        logger.e(videoStopped.arguments);
      }
    } else {
      MethodChannelResponse? videoStart = await methodChannelProvider
          ?.callMethod(MethodCallOption.localVideoOn);
      if (videoStart == null) {
        logger.e(Response.video_start_response_null);
        return;
      }

      if (videoStart.result) {
        logger.i(videoStart.arguments);
      } else {
        logger.e(videoStart.arguments);
      }
    }
  }

  void stopMeeting() async {
    MethodChannelResponse? stopResponse =
        await methodChannelProvider?.callMethod(MethodCallOption.stop);
    if (stopResponse == null) {
      logger.e(Response.stop_response_null);
      return;
    }
    logger.i(stopResponse.arguments);
  }

  //
  // —————————————————————————— Helpers ——————————————————————————————————————
  //

  void _resetMeetingValues() {
    meetingId = null;
    meetingData = null;
    localAttendeeId = null;
    remoteAttendeeId = null;
    contentAttendeeId = null;
    selectedAudioDevice = null;
    audioDeviceList = [];
    videoDeviceList = [];
    currAttendees = {};
    isReceivingScreenShare = false;
    isMeetingActive = false;
    isMicOn = true;
    isVideoOn = false;
    isNoiseCancellationEnabled = false;

    logger.i("Meeting values reset");
    notifyListeners();
  }

  String formatExternalUserId(String? externalUserId) {
    List<String>? externalUserIdArray = externalUserId?.split("#");
    if (externalUserIdArray == null) {
      return "UNKNOWN";
    }
    String extUserId =
        externalUserIdArray.length == 2 ? externalUserIdArray[1] : "UNKNOWN";
    return extUserId;
  }

  bool _isAttendeeContent(String? attendeeId) {
    List<String>? attendeeIdArray = attendeeId?.split("#");
    return attendeeIdArray?.length == 2;
  }

  void invokeNotifyListeners() {
    notifyListeners();
  }
}
