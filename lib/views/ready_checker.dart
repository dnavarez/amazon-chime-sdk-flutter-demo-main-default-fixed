import 'dart:io' show Platform;

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_aws_chime_plugin/objects/method_channel_coordinator.dart';
import 'package:provider/provider.dart';

import '../view_models/join_meeting_view_model.dart';
import '../view_models/meeting_view_model.dart';
import 'meeting.dart';
import 'style.dart';

class ReadyCheckerView extends StatelessWidget {
  const ReadyCheckerView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final joinMeetingProvider = Provider.of<JoinMeetingViewModel>(context);
    final methodChannelProvider =
        Provider.of<MethodChannelCoordinator>(context);
    final meetingProvider = Provider.of<MeetingViewModel>(context);

    final orientation = MediaQuery.of(context).orientation;

    return joinMeetingBody(joinMeetingProvider, methodChannelProvider,
        meetingProvider, context, orientation);
  }

//
// —————————————————————————— Main Body ——————————————————————————————————————
//

  Widget joinMeetingBody(
      JoinMeetingViewModel joinMeetingProvider,
      MethodChannelCoordinator methodChannelProvider,
      MeetingViewModel meetingProvider,
      BuildContext context,
      Orientation orientation) {
    return joinMeetingBodyPortrait(joinMeetingProvider, orientation,
        methodChannelProvider, meetingProvider, context);
  }

//
// —————————————————————————— Portrait Body ——————————————————————————————————————
//

  Widget joinMeetingBodyPortrait(
      JoinMeetingViewModel joinMeetingProvider,
      Orientation orientation,
      MethodChannelCoordinator methodChannelProvider,
      MeetingViewModel meetingProvider,
      BuildContext context) {
    // meetingProvider.selectedVideoDevice ??= meetingProvider.videoDeviceList.first;

    return Scaffold(
      appBar: AppBar(
        title: const Text('Select Devices'),
      ),
      body: Center(
        // child: SizedBox(
        //   height: 200,
        //   width: 200,
        //   child: PlatformViewLink(
        //     viewType: 'ChimeDefaultVideoRenderView',
        //     surfaceFactory:
        //         (BuildContext context, PlatformViewController controller) {
        //       return AndroidViewSurface(
        //         controller: controller as AndroidViewController,
        //         gestureRecognizers: const <Factory<
        //             OneSequenceGestureRecognizer>>{},
        //         hitTestBehavior: PlatformViewHitTestBehavior.transparent,
        //       );
        //     },
        //     onCreatePlatformView: (PlatformViewCreationParams params) {
        //       final AndroidViewController controller =
        //           PlatformViewsService.initExpensiveAndroidView(
        //         id: params.id,
        //         viewType: params.viewType,
        //         layoutDirection: TextDirection.ltr,
        //         creationParams: null,
        //         creationParamsCodec: const StandardMessageCodec(),
        //         onFocus: () => params.onFocusChanged,
        //       );
        //       controller.addOnPlatformViewCreatedListener(
        //           params.onPlatformViewCreated);
        //       controller.create();

        //       return controller;
        //     },
        //   ),
        // ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              height: 8,
            ),
            videoPreview(meetingProvider, orientation, context),
            Positioned(
              top: -20.5,
              right: 30,
              child: switchIcon(meetingProvider),
            ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   crossAxisAlignment: CrossAxisAlignment.center,
            //   children: [
            //       displayVideoTiles(meetingProvider, orientation, context),
            //       Icon(localVideoIcon(meetingProvider)),
            //   ],
            // ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 32, vertical: 10),
                  child: videoOnIcon(meetingProvider),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 32, vertical: 10),
                  child: audioOnIcon(meetingProvider),
                ),
              ],
            ),
            textWidget("Video Filter", 32),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 10),
              child: videoFilterDropDown(meetingProvider),
            ),
            textWidget("Speaker", 32),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 10),
              child: speakerDropDown(meetingProvider),
            ),
            speakerTestButton(joinMeetingProvider, methodChannelProvider,
                meetingProvider, context),
            // textWidget("Video Device", 32),
            // Padding(
            //   padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 10),
            //   child: cameraDropDown(meetingProvider),
            // ),
            // textWidget("Video Format", 32),
            // Padding(
            //   padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 10),
            //   child: videoFormatDropDown(meetingProvider),
            // ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                noiseCancellation(meetingProvider),
                textWidget("NoiseCancellation", 14),
              ],
            ),
            joinButton(joinMeetingProvider, methodChannelProvider,
                meetingProvider, context),
            loadingIcon(joinMeetingProvider),
          ],
        ),
      ),
    );
  }

//
// —————————————————————————— Helpers ——————————————————————————————————————
//

  Widget joinButton(
      JoinMeetingViewModel joinMeetingProvider,
      MethodChannelCoordinator methodChannelProvider,
      MeetingViewModel meetingProvider,
      BuildContext context) {
    return ElevatedButton(
      child: const Text("Join Meeting"),
      onPressed: () async {
        if (!joinMeetingProvider.joinButtonClicked) {
          // Prevent multiple clicks
          joinMeetingProvider.joinButtonClicked = true;

          // Hide Keyboard
          FocusManager.instance.primaryFocus?.unfocus();

          // Call api, format to JSON and send to native
          bool isMeetingJoined = await joinMeetingProvider.joinMeeting(
              meetingProvider, methodChannelProvider);

          if (isMeetingJoined) {
            if (meetingProvider.isNoiseCancellationEnabled) {
              meetingProvider.updateNoiseCancellationStatus();
            }

            if (!meetingProvider.isMicOn) {
              meetingProvider.sendLocalMuteToggle();
            }

            if (meetingProvider.isVideoOn) {
              meetingProvider.sendLocalVideoTileOn();
            }

            // ignore: use_build_context_synchronously
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const MeetingView(),
              ),
            );
          }
          joinMeetingProvider.joinButtonClicked = false;
        }
      },
    );
  }

  Widget speakerTestButton(
      JoinMeetingViewModel joinMeetingProvider,
      MethodChannelCoordinator methodChannelProvider,
      MeetingViewModel meetingProvider,
      BuildContext context) {
    return ElevatedButton(
      child: const Text("Test"),
      onPressed: () async {
        meetingProvider.testSpeaker();
      },
    );
  }

  Widget noiseCancellation(MeetingViewModel meetingProvider) {
    return Checkbox(
      value: meetingProvider.isNoiseCancellationEnabled,
      onChanged: (value) {
        meetingProvider.setNoiseCancellationState(value ?? false);
      },
    );
  }

  Widget speakerDropDown(MeetingViewModel meetingProvider) {
    return DropdownMenu<String>(
      initialSelection: meetingProvider.audioDeviceList.isNotEmpty
          ? meetingProvider.selectedAudioDevice
          : null,
      onSelected: (String? value) {
        meetingProvider.updateCurrentAudioDevice(value!);
      },
      dropdownMenuEntries: meetingProvider.audioDeviceList
          .map<DropdownMenuEntry<String>>((String? value) {
        return DropdownMenuEntry<String>(
            value: value ?? '', label: value ?? '');
      }).toList(),
    );
  }

  Widget cameraDropDown(MeetingViewModel meetingProvider) {
    return DropdownMenu<String>(
      initialSelection: meetingProvider.videoDeviceList.isNotEmpty
          ? meetingProvider.videoDeviceList.first
          : null,
      onSelected: (String? value) {
        meetingProvider.updateCurrentVideoDevice(value!);
      },
      dropdownMenuEntries: meetingProvider.videoDeviceList
          .map<DropdownMenuEntry<String>>((String? value) {
        return DropdownMenuEntry<String>(
            value: value ?? '', label: value ?? '');
      }).toList(),
    );
  }

  Widget videoFormatDropDown(MeetingViewModel meetingProvider) {
    return DropdownMenu<String>(
      initialSelection: meetingProvider.videoFormatList.isNotEmpty
          ? meetingProvider.videoFormatList.first
          : null,
      onSelected: (String? value) {
        // This is called when the user selects an item.
        // setState(() {
        //   dropdownValue = value!;
        // });
      },
      dropdownMenuEntries: meetingProvider.videoFormatList
          .map<DropdownMenuEntry<String>>((String? value) {
        return DropdownMenuEntry<String>(
            value: value ?? '', label: value ?? '');
      }).toList(),
    );
  }

  Widget videoFilterDropDown(MeetingViewModel meetingProvider) {
    return DropdownMenu<String>(
      initialSelection: meetingProvider.bgBlurStrengthList.isNotEmpty
          ? meetingProvider.bgBlurStrengthList.first
          : null,
      onSelected: (String? value) {
        meetingProvider.updateVideoFilter(value!);
      },
      dropdownMenuEntries: meetingProvider.bgBlurStrengthList
          .map<DropdownMenuEntry<String>>((String? value) {
        return DropdownMenuEntry<String>(
            value: value ?? '', label: value ?? '');
      }).toList(),
    );
  }

  Widget loadingIcon(JoinMeetingViewModel joinMeetingProvider) {
    if (joinMeetingProvider.loadingStatus) {
      return const Padding(
          padding: EdgeInsets.symmetric(vertical: 10),
          child: CircularProgressIndicator());
    } else {
      return const SizedBox.shrink();
    }
  }

  Widget textWidget(String str, double fontSize) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: Text(
        str,
        style: TextStyle(
          fontSize: fontSize,
          color: Colors.blue,
        ),
      ),
    );
  }

  Widget audioOnIcon(MeetingViewModel meetingProvider) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: IconButton(
        icon: Icon(localMuteIcon(meetingProvider)),
        iconSize: Style.iconSize,
        padding: EdgeInsets.symmetric(horizontal: Style.iconPadding),
        constraints: const BoxConstraints(),
        color: Colors.blue,
        onPressed: () {
          meetingProvider.setMicState();
        },
      ),
    );
  }

  Widget videoOnIcon(MeetingViewModel meetingProvider) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: IconButton(
        icon: Icon(localVideoIcon(meetingProvider)),
        iconSize: Style.iconSize,
        padding: EdgeInsets.symmetric(horizontal: Style.iconPadding),
        constraints: const BoxConstraints(),
        color: Colors.blue,
        onPressed: () {
          meetingProvider.setVideoOn();
        },
      ),
    );
  }

  Widget switchIcon(MeetingViewModel meetingProvider) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4),
      child: IconButton(
        icon: Icon(localSwitchIcon(meetingProvider)),
        iconSize: Style.iconSize,
        padding: EdgeInsets.symmetric(horizontal: Style.iconPadding),
        constraints: const BoxConstraints(),
        color: Colors.blue,
        onPressed: () {
          meetingProvider.switchCamera();
        },
      ),
    );
  }

  IconData localVideoIcon(MeetingViewModel meetingProvider) {
    if (meetingProvider.isVideoOn) {
      return Icons.videocam;
    } else {
      return Icons.videocam_off;
    }
  }

  IconData localMuteIcon(MeetingViewModel meetingProvider) {
    if (meetingProvider.isMicOn) {
      return Icons.mic;
    } else {
      return Icons.mic_off;
    }
  }

  IconData localSwitchIcon(MeetingViewModel meetingProvider) {
    return Icons.switch_camera;
  }

  // Video Tile

  Widget videoPreview(MeetingViewModel meetingProvider, Orientation orientation,
      BuildContext context) {
    Widget localVideoTile = videoTile(meetingProvider);

    if (meetingProvider.isVideoOn) {
      return localVideoTile;
    } else {
      const Widget emptyVideos = Text("No video detected");
      if (orientation == Orientation.portrait) {
        return emptyVideos;
      } else {
        return const Center(
          widthFactor: 2.5,
          child: emptyVideos,
        );
      }
    }
  }

  Widget videoTile(MeetingViewModel meetingProvider) {
    // int? paramsVT;

    // paramsVT = meetingProvider.currAttendees[meetingProvider.localAttendeeId]?.videoTile?.tileId;

    Widget videoTile;
    if (Platform.isIOS) {
      videoTile = const UiKitView(
        viewType: "videoPreview",
        creationParams: null,
        creationParamsCodec: StandardMessageCodec(),
      );
    } else if (Platform.isAndroid) {
      // videoTile = AndroidView(
      //     viewType: 'VideoPreviewView',
      //     onPlatformViewCreated: (int viewId) {
      //       // meetingProvider.invokeNotifyListeners();
      //       //   controller
      //       //     .addOnPlatformViewCreatedListener(params.onPlatformViewCreated);
      //       // controller.create();
      //       // return controller;
      //     });
      videoTile = PlatformViewLink(
        viewType: 'VideoPreviewView',
        surfaceFactory:
            (BuildContext context, PlatformViewController controller) {
          return AndroidViewSurface(
            controller: controller as AndroidViewController,
            gestureRecognizers: const <Factory<OneSequenceGestureRecognizer>>{},
            hitTestBehavior: PlatformViewHitTestBehavior.opaque,
          );
        },
        onCreatePlatformView: (PlatformViewCreationParams params) {
          final AndroidViewController controller =
              PlatformViewsService.initSurfaceAndroidView(
            id: params.id,
            viewType: params.viewType,
            layoutDirection: TextDirection.ltr,
            // creationParams: params.id,
            // creationParamsCodec: const StandardMessageCodec(),
            // onFocus: () => params.onFocusChanged,
          );
          controller
              .addOnPlatformViewCreatedListener(params.onPlatformViewCreated);
          controller.create();
          

          return controller;
        },
      );
    } else {
      videoTile = const Text("Unrecognized Platform.");
    }

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4),
      child: SizedBox(
        width: 200,
        height: 230,
        child: videoTile,
      ),
    );
  }
}
