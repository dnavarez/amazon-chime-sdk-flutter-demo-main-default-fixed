/*
 * Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * SPDX-License-Identifier: MIT-0
 */

class AudioDevicesInterface {
  void initialAudioSelection() {
    // Gets initial selected audio device
  }

  void listAudioDevices() async {
    // Gets a list of available audio devices.
  }

  void listVideoDevices() async {
    // Gets a list of available video devices.
  }

  void listVideoFormats() async {
    // Gets a list of available video formats.
  }

  void updateCurrentAudioDevice(String device) async {
    // Updates the current audio device to the chosen audio device.
  }

  void updateCurrentVideoDevice(String device) async {
    // Updates the current video device to the chosen audio device.
  }

  void updateCurrentVideoFormat(String device) async {
    // Updates the current video format to the chosen audio device.
  }

  void updateNoiseCancellationStatus() async {
    // Updates the noise cancellation status
  }

  void testSpeaker() async {
    // Test speaker selected
  }

  void listBackgroundBlurStrength() async {
    // Gets a list of available background strength.
  }

  void updateVideoFilter(String videoFilter) async {
    // Update video filter
  }
}
