/*
 * Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * SPDX-License-Identifier: MIT-0
 */
 
package com.amazonaws.services.chime.flutterdemo

enum class Response(val msg: String) {
    // Authorization
    audio_auth_granted("Android: Audio usage authorized."),
    audio_auth_not_granted("Android: Failed to authorize audio."),
    video_auth_granted("Android: Video usage authorized."),
    video_auth_not_granted("Android: Failed to authorize video."),

    // Meeting
    incorrect_join_response_params("Android: ERROR api response has incorrect/missing parameters."),
    create_meeting_success("Android: meetingSession created successfully."),
    create_meeting_failed ("Android: ERROR failed to create meetingSession."),
    join_meeting_success("Android: meetingSession join successfully."),
    meeting_stopped_successfully("Android: meetingSession stopped successfully."),
    meeting_session_is_null("Android: ERROR Meeting session is null."),
    meeting_start_failed("Android: ERROR failed to start meeting."),
    join_meeting_failed("Android: ERROR failed to join meeting."),

    // Mute
    mute_successful("Android: Successfully muted user"),
    mute_failed("Android: ERROR failed to mute user"),
    unmute_successful("Android: Successfully unmuted user"),
    unmute_failed("Android: ERROR failed to unmute user"),

    // Video
    local_video_on_success("Android: Started local video."),
    video_device_updated("Android: Video device updated."),
    local_video_on_failed("Android: ERROR could not start local video."),
    local_video_off_success("Android: Stopped local video."),
    video_device_update_failed("Android: Failed to update video device."),
    failed_to_list_video_devices("Android: ERROR failed to list video devices."),

    // Video Format
    video_format_updated("Android: Video format updated."),
    video_format_update_failed("Android: Failed to update video format."),
    failed_to_list_video_formats("Android: ERROR failed to list video formats."),

    // Audio Device
    audio_device_updated("Android: Audio device updated"),
    audio_device_update_failed("Android: Failed to update audio device."),
    null_audio_device("Android: ERROR received null as audio device."),
    failed_to_get_initial_audio_device("Android: Failed to get initial audio device"),
    failed_to_list_audio_devices("Android: ERROR failed to list audio devices."),

    // Noise Cancellation
    noise_cancellation_updated("Android: Noise cancellation updated."),
    noise_cancellation_failed("Android: ERROR failed to update noise cancellation."),

    // Method Channel
    method_not_implemented("Android: ERROR method not implemented."),

    // Speaker Channel
    speaker_test_successful("Android: Speaker test successfully"),
    speaker_test_failed("Android: Speaker test failed")
}
