/*
 * Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * SPDX-License-Identifier: MIT-0
 */

package com.amazonaws.services.chime.flutterdemo

import android.content.Context
import com.amazonaws.services.chime.sdk.meetings.session.DefaultMeetingSession
import com.amazonaws.services.chime.sdk.meetings.audiovideo.AudioVideoObserver
import com.amazonaws.services.chime.sdk.meetings.audiovideo.AudioVideoFacade
import com.amazonaws.services.chime.sdk.meetings.utils.logger.ConsoleLogger
import com.amazonaws.services.chime.sdk.meetings.audiovideo.video.capture.DefaultCameraCaptureSource
import com.amazonaws.services.chime.sdk.meetings.audiovideo.video.gl.DefaultEglCoreFactory
import com.amazonaws.services.chime.sdk.meetings.audiovideo.video.capture.DefaultSurfaceTextureCaptureSourceFactory

import android.util.Log
object MeetingSessionManager {
    private val meetingSessionlogger: ConsoleLogger = ConsoleLogger()
    val eglCoreFactory: DefaultEglCoreFactory  = DefaultEglCoreFactory()
    var cameraCaptureSource: DefaultCameraCaptureSource? = null
    var realtimeObserver: RealtimeObserver? = null
    var videoTileObserver: VideoTileObserver? = null
    var audioVideoObserver: AudioVideoObserver? = null

    var meetingSession: DefaultMeetingSession? = null


    private val NULL_MEETING_SESSION_RESPONSE: MethodChannelResult =
        MethodChannelResult(false, Response.meeting_session_is_null.msg)

    fun joinMeetingAudio(
        context: Context,
        realtimeObserver: RealtimeObserver? = null,
        videoTileObserver: VideoTileObserver? = null,
        audioVideoObserver: AudioVideoObserver? = null
    ): MethodChannelResult {
        addObservers(realtimeObserver, videoTileObserver, audioVideoObserver)

        val surfaceTextureCaptureSourceFactory = DefaultSurfaceTextureCaptureSourceFactory(
            ConsoleLogger(),
           eglCoreFactory
       )

        cameraCaptureSource =
            DefaultCameraCaptureSource(
                context,
                meetingSessionlogger,
                surfaceTextureCaptureSourceFactory
            )
        return MethodChannelResult(true, Response.join_meeting_success.msg)
    }

    fun startMeeting(): MethodChannelResult {
        val audioVideo: AudioVideoFacade = meetingSession?.audioVideo ?: return NULL_MEETING_SESSION_RESPONSE

        audioVideo.start()
        audioVideo.startRemoteVideo()
        return MethodChannelResult(true, Response.create_meeting_success.msg)
    }

    fun stop(): MethodChannelResult {
        meetingSession?.audioVideo?.stopRemoteVideo() ?: return NULL_MEETING_SESSION_RESPONSE
        meetingSession?.audioVideo?.stop() ?: return NULL_MEETING_SESSION_RESPONSE
        removeObservers()
        meetingSession = null
        return MethodChannelResult(true, Response.meeting_stopped_successfully.msg)
    }

    private fun addObservers(
        realtimeObserver: RealtimeObserver?,
        videoTileObserver: VideoTileObserver?,
        audioVideoObserver: AudioVideoObserver?
    ) {
        val audioVideo: AudioVideoFacade = meetingSession?.audioVideo ?: return
        realtimeObserver?.let {
            audioVideo.addRealtimeObserver(it)
            this.realtimeObserver = realtimeObserver
            meetingSessionlogger.debug("RealtimeObserver", "RealtimeObserver initialized")
        }
        audioVideoObserver?.let {
            audioVideo.addAudioVideoObserver(it)
            this.audioVideoObserver = audioVideoObserver
            meetingSessionlogger.debug("AudioVideoObserver", "AudioVideoObserver initialized")
        }
        videoTileObserver?.let {
            audioVideo.addVideoTileObserver(videoTileObserver)
            this.videoTileObserver = videoTileObserver
            meetingSessionlogger.debug("VideoTileObserver", "VideoTileObserver initialized")
        }
    }

    private fun removeObservers() {
        realtimeObserver?.let {
            meetingSession?.audioVideo?.removeRealtimeObserver(it)
        }
        audioVideoObserver?.let {
            meetingSession?.audioVideo?.removeAudioVideoObserver(it)
        }
        videoTileObserver?.let {
            meetingSession?.audioVideo?.removeVideoTileObserver(it)
        }
    }
}
