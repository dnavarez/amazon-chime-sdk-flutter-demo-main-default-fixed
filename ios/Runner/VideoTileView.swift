/*
 * Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * SPDX-License-Identifier: MIT-0
 */

import AmazonChimeSDK
import Foundation

class VideoTileView: NSObject, FlutterPlatformView {
  private var _view: UIView
  
  var backgroundBlurProcessor: BackgroundBlurVideoFrameProcessor?
  var cameraCaptureSource = MeetingSession.shared.cameraCaptureSource
  
  init(
    frame: CGRect,
    viewIdentifier viewId: Int64,
    arguments args: Any?
  ) {
    _view = DefaultVideoRenderView()
    super.init()
    
    // Receieve tileId as a param.
    let tileId = args as! Int
    let videoRenderView = _view as! VideoRenderView
    
    // Bind view to VideoView
//    MeetingSession.shared.meetingSession?.audioVideo.bindVideoView(videoView: videoRenderView, tileId: tileId)
    
    cameraCaptureSource.start()
    
    if MeetingSession.shared.backgroundBlurStrength == nil {
      removeCameraCaptureSourceBackgroundBlur()
      cameraCaptureSource.addVideoSink(sink: videoRenderView)
    } else {
      setupBGBlur()
      applyBlur()
    }
    
    MeetingSession.shared.meetingSession?.audioVideo.startLocalVideo(source: cameraCaptureSource)
    
    // Fix aspect ratio
    _view.contentMode = .scaleAspectFit
    
    // Declare _view as UIView for Flutter interpretation
    _view = _view as UIView
  }
  
  func view() -> UIView {
    return _view
  }
  
  func setupBGBlur() {
    let backgroundBlurConfigurations = BackgroundBlurConfiguration(
      logger: ConsoleLogger(name: "BackgroundBlurProcessor"),
      blurStrength: BackgroundBlurStrength.high
    )
    
    backgroundBlurProcessor = BackgroundBlurVideoFrameProcessor(
      backgroundBlurConfiguration: backgroundBlurConfigurations
    )
    
  }
  
  func applyBlur() {
    let videoRenderView = _view as! VideoRenderView
    
    cameraCaptureSource.removeVideoSink(sink: videoRenderView)
    
    backgroundBlurProcessor?.addVideoSink(sink: videoRenderView)
    cameraCaptureSource.addVideoSink(sink: backgroundBlurProcessor!)
  }
  
  func removeCameraCaptureSourceBackgroundBlur() {
    guard let backgroundBlurProcessor = backgroundBlurProcessor else { return }
    cameraCaptureSource.removeVideoSink(sink: backgroundBlurProcessor)
  }
}
