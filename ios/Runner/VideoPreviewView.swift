//
//  VideoPreviewView.swift
//  Runner
//
//  Created by Dan Navarez on 10/17/23.
//

import AmazonChimeSDK
import Foundation
import AVFoundation

class VideoPreviewView: NSObject, FlutterPlatformView {
  private var _view: UIView
  
  var backgroundBlurProcessor: BackgroundBlurVideoFrameProcessor?
  var cameraCaptureSource = MeetingSession.shared.cameraCaptureSource
  
  init(
    frame: CGRect,
    viewIdentifier viewId: Int64
  ) {
    _view = DefaultVideoRenderView()
    super.init()
    
    let videoRenderView = _view as! VideoRenderView
    
    cameraCaptureSource.start()
    
    if MeetingSession.shared.backgroundBlurStrength == nil {
      removeCameraCaptureSourceBackgroundBlur()
      cameraCaptureSource.addVideoSink(sink: videoRenderView)
    } else {
      setupBGBlur()
      applyBlur()
    }
    
    // Fix aspect ratio
    _view.contentMode = .scaleAspectFit
    
    // Declare _view as UIView for Flutter interpretation
    _view = _view as UIView
  }
  
  func view() -> UIView {
    return _view
  }
  
  func setupBGBlur() {
    let backgroundBlurConfigurations = BackgroundBlurConfiguration(
      logger: ConsoleLogger(name: "BackgroundBlurProcessor"),
      blurStrength: MeetingSession.shared.backgroundBlurStrength ?? .low
    )
    
    backgroundBlurProcessor = BackgroundBlurVideoFrameProcessor(
      backgroundBlurConfiguration: backgroundBlurConfigurations
    )
    
  }
  
  func applyBlur() {
    let videoRenderView = _view as! VideoRenderView
    
//    cameraCaptureSource.removeVideoSink(sink: videoRenderView)
    
    backgroundBlurProcessor?.addVideoSink(sink: videoRenderView)
    cameraCaptureSource.addVideoSink(sink: backgroundBlurProcessor!)
  }
  
  func removeCameraCaptureSourceBackgroundBlur() {
    guard let backgroundBlurProcessor = backgroundBlurProcessor else { return }
    cameraCaptureSource.removeVideoSink(sink: backgroundBlurProcessor)
  }
}

