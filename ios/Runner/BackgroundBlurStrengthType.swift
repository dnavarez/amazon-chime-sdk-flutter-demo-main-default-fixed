//
//  BackgroundBlurStrengthType.swift
//  Runner
//
//  Created by Dan Navarez on 11/10/23.
//

import Foundation
import AmazonChimeSDK

enum BackgroundBlurStrengthType: String, CaseIterable {
  case low = "Blur - low"
  case medium = "Blur - medium"
  case high = "Blur - high"
  
  var amazonBGBlurStrength: BackgroundBlurStrength? {
    switch self {
    case .low:
      return .low
    case .medium:
      return .medium
    case .high:
      return .high
    }
  }
}

