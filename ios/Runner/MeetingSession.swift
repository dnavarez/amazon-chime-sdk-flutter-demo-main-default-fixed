/*
 * Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
 * SPDX-License-Identifier: MIT-0
 */

import AmazonChimeSDK
import AmazonChimeSDKMedia
import AVFoundation
import Flutter
import Foundation

// Singleton Pattern Class
class MeetingSession {
  static let shared = MeetingSession()
  
  var meetingSession: DefaultMeetingSession?
  
  var cameraCaptureSource = DefaultCameraCaptureSource(logger: ConsoleLogger(name: "CustomCameraSource"))
  var backgroundBlurProcessor: BackgroundBlurVideoFrameProcessor?
  var backgroundBlurStrength: BackgroundBlurStrength? = nil
  
  let audioVideoConfig = AudioVideoConfiguration()
  private let logger = ConsoleLogger(name: "MeetingSession")
  
  private init() {}
  
  func joinMeetingAudio() -> MethodChannelResponse {
    let audioSessionConfigured = configureAudioSession()
    if audioSessionConfigured {
//      setupCameraCaptureSource()
      return MethodChannelResponse(result: true, arguments: Response.join_meeting_success.rawValue)
    }
    return MethodChannelResponse(result: false, arguments: Response.join_meeting_failed.rawValue)
  }
  
  func startMeeting() -> MethodChannelResponse {
    let audioSessionStarted = startAudioVideoConnection()
    if audioSessionStarted {
      return MethodChannelResponse(result: true, arguments: Response.create_meeting_success.rawValue)
    }
    return MethodChannelResponse(result: false, arguments: Response.create_meeting_failed.rawValue)
  }
  
//  private func setupCameraCaptureSource() {
//    cameraCaptureSource = DefaultCameraCaptureSource(logger: ConsoleLogger(name: "CustomCameraSource"))
//    cameraCaptureSource.setEventAnalyticsController(eventAnalyticsController: meetingSession?.eventAnalyticsController)
//  }
//  
//  func addCustomCameraCaptureSourceBackgroundBlurWith(videoRenderView: VideoRenderView) {
//    guard let backgroundBlurStrength = backgroundBlurStrength else { return }
//    
//    cameraCaptureSource.removeVideoSink(sink: videoRenderView)
//    
//    let backgroundBlurConfigurations = BackgroundBlurConfiguration(
//      logger: ConsoleLogger(name: "BackgroundBlurProcessor"),
//      blurStrength: BackgroundBlurStrength.high
//    )
//    let bgBlurProcessor = BackgroundBlurVideoFrameProcessor(
//      backgroundBlurConfiguration: backgroundBlurConfigurations
//    )
//    
//    bgBlurProcessor.addVideoSink(sink: videoRenderView)
//    
//    backgroundBlurProcessor = bgBlurProcessor
//    
//    cameraCaptureSource.addVideoSink(sink: backgroundBlurProcessor!)
//  }
//  
//  func removeCameraCaptureSourceBackgroundBlur() {
//    guard let backgroundBlurProcessor = backgroundBlurProcessor else { return }
//    cameraCaptureSource.removeVideoSink(sink: backgroundBlurProcessor)
//  }
  
  private func startAudioVideoConnection() -> Bool {
    do {
      try meetingSession?.audioVideo.start()
      meetingSession?.audioVideo.startRemoteVideo()
    } catch PermissionError.audioPermissionError {
      logger.error(msg: "Audio permissions error.")
      return false
    } catch {
      logger.error(msg: "Error starting the Meeting: \(error.localizedDescription)")
      return false
    }
    return true
  }
  
  private func configureAudioSession() -> Bool {
    let audioSession = AVAudioSession.sharedInstance()
    do {
      try audioSession.setActive(true, options: .notifyOthersOnDeactivation)
      try audioSession.setMode(.voiceChat)
    } catch {
      logger.error(msg: "Error configuring AVAudioSession: \(error.localizedDescription)")
      return false
    }
    return true
  }
}
